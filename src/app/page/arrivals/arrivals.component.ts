import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'app-arrivals',
    templateUrl: './arrivals.component.html',
    styleUrls: ['./arrivals.component.scss']
})
export class ArrivalsComponent implements OnInit {

    constructor(private httpClient: HttpClient, private route: ActivatedRoute) { }

    private coords: string;
    private arrivals: any[];

    private setCoords(): void {
        this.coords = encodeURIComponent(this.route.snapshot.params.coords);
    }

    ngOnInit() {
        this.setCoords();

        this.httpClient
            .get(`https://api.navitia.io/v1/coverage/fr-idf/coord/${this.coords}/arrivals?`,
                {
                    headers: new HttpHeaders().append('Authorization', 'be1cb4bf-77a4-4250-baa5-19ff8f8a77af')
                })
            .subscribe(
                (poisResponse) => {
                    this.arrivals = poisResponse['arrivals'];
                    console.log(poisResponse)
                },
            );
    }

}
