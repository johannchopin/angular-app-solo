import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Station } from 'src/app/interface/station';
import { Company } from 'src/app/interface/company';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    private pois: [];

    constructor(private httpClient: HttpClient) { }

    private setStations(): void {
        this.httpClient
            .get('https://api.navitia.io/v1/coverage/fr-idf/pois?',
                {
                    headers: new HttpHeaders().append('Authorization', 'be1cb4bf-77a4-4250-baa5-19ff8f8a77af')
                })
            .subscribe(
                (PoisResponse) => {
                    console.log(PoisResponse);
                    this.pois = PoisResponse['pois'];
                },
            );
    }

    private getCoverPath(coverId: string): string {
        return `http://covers.openlibrary.org/b/id/${coverId}-M.jpg`;
    }


    ngOnInit(): void {
        this.setStations();
    }
}
