import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-departures',
    templateUrl: './departures.component.html',
    styleUrls: ['./departures.component.scss']
})
export class DeparturesComponent implements OnInit {

    constructor(private httpClient: HttpClient, private route: ActivatedRoute) { }

    private coords: string;
    private departures: any[];

    private setCoords(): void {
        this.coords = encodeURIComponent(this.route.snapshot.params.coords);
        console.log(this.coords);
    }

    ngOnInit() {
        this.setCoords();

        this.httpClient
            .get(`https://api.navitia.io/v1/coverage/fr-idf/coord/${this.coords}/departures?`,
                {
                    headers: new HttpHeaders().append('Authorization', 'be1cb4bf-77a4-4250-baa5-19ff8f8a77af')
                })
            .subscribe(
                (poisResponse) => {
                    this.departures = poisResponse['departures'];
                    console.log(poisResponse)
                },
            );
    }

}
