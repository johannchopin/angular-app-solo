import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
    selector: 'app-contacts',
    templateUrl: './contacts.component.html',
    styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

    constructor(private formsModule: FormsModule) { }

    private invalidsFields: string[];
    private name: string;
    private surname: string;
    private need: string;
    private email: string;
    private message: string;

    ngOnInit() {
    }

    private processForm(): void {
        if (this.areUserDataValid()) {
            $('#userDataModal').modal('show');
        }
    }

    private areUserDataValid(): boolean {
        const invalidFields = [];

        if (this.name === undefined || this.name.trim() === '') {
            invalidFields.push('Your name');
        }
        if (this.surname === undefined || this.surname.trim() === '') {
            invalidFields.push('Your surname');
        }
        if (this.need === undefined || this.need.trim() === '') {
            invalidFields.push('Your need');
        }
        if (this.email === undefined || this.email.trim() === '') {
            invalidFields.push('Your email');
        }
        if (this.message === undefined || this.message.trim() === '') {
            invalidFields.push('a message');
        }

        if (invalidFields.length > 0) {
            this.showInvalidsFieldsModal(invalidFields);
            return false;
        }

        return true;
    }

    private showInvalidsFieldsModal(invalidFields: string[]) {
        this.invalidsFields = invalidFields;
        $('#invalidsFieldsModal').modal('show');
    }
}
