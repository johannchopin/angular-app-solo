import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Company } from 'src/app/interface/company';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  private companies: Company[];

  private setCompanies(): void {
    this.httpClient
      .get('https://api.navitia.io/v1/coverage/fr-idf/companies/',
        {
          headers: new HttpHeaders().append('Authorization', 'be1cb4bf-77a4-4250-baa5-19ff8f8a77af')
        })
      .subscribe(
        (stationsResponse) => {
          console.log(stationsResponse);
          this.companies = stationsResponse['companies'] as Company[];
        },
      );
  }
  ngOnInit() {
    this.setCompanies();
  }

}
