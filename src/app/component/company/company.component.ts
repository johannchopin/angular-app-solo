import { Component, OnInit, Input } from '@angular/core';
import { faBuilding } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})

export class CompanyComponent implements OnInit {

  constructor() { }

  private faBuilding = faBuilding;

  @Input() id: string;
  @Input() name: string;

  ngOnInit() {
  }

}
