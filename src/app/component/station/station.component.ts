import { Component, OnInit, Input } from '@angular/core';
import { faHotel } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.scss']
})
export class StationComponent implements OnInit {

  private faHotel = faHotel;
  constructor() { }

  @Input() name: string;
  @Input() code: string;

  ngOnInit() {
  }

}
