import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StationDisplayInfosComponent } from './station-display-infos.component';

describe('StationDisplayInfosComponent', () => {
  let component: StationDisplayInfosComponent;
  let fixture: ComponentFixture<StationDisplayInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StationDisplayInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StationDisplayInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
