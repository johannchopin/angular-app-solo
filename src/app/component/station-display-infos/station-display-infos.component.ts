import { Component, OnInit, Input } from '@angular/core';
import { StationDisplayInfos } from 'src/app/interface/station-display-infos';
import { IconDefinition, faRoad, faBus, faPlane, faTrain, faShip } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-station-display-infos',
    templateUrl: './station-display-infos.component.html',
    styleUrls: ['./station-display-infos.component.scss']
})
export class StationDisplayInfosComponent implements OnInit {

    constructor() { }

    private physicalModeIcon: IconDefinition;

    @Input() infos: StationDisplayInfos;

    private setPhysicalModeIcon() {
        let physicalModeIcon = faRoad;

        switch (this.infos.physical_mode) {
            case 'Bus':
                physicalModeIcon = faBus;
                break;

            case 'Air':
                physicalModeIcon = faPlane;
                break;

            case 'Train':
                physicalModeIcon = faTrain;
                break;

            case 'Boat':
                physicalModeIcon = faShip;
                break;
        }

        this.physicalModeIcon = physicalModeIcon;

    }

    ngOnInit() {
        this.setPhysicalModeIcon();
    }

}
