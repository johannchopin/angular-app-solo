import { Component, OnInit, Input } from '@angular/core';
import { IconDefinition, faMapMarkerAlt, faBicycle, faParking } from '@fortawesome/free-solid-svg-icons';

import { Poi } from 'src/app/interface/poi';

@Component({
    selector: 'app-poi',
    templateUrl: './poi.component.html',
    styleUrls: ['./poi.component.scss']
})
export class PoiComponent implements OnInit {

    constructor() { }

    private poiIcon: IconDefinition;
    private mapMarkerIcon = faMapMarkerAlt;

    @Input() poi: Poi;

    private setPoiIcon(): void {
        let poiIcon = faMapMarkerAlt;

        switch (this.poi.poi_type.name) {
            // TODO: Add some types

            case 'Parking vélo':
                poiIcon = faBicycle;
                break;

            case 'Parking':
                poiIcon = faParking;
                break;
        }

        this.poiIcon = poiIcon;
    }

    ngOnInit() {
        this.setPoiIcon();
    }

}
