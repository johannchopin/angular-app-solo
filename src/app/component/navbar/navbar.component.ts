import { Component, OnInit } from '@angular/core';
import { faRoute } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  faRoute = faRoute;

  constructor() { }

  ngOnInit() {
  }

}
