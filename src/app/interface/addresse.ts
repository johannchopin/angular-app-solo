export interface Addresse {
    house_number: number;
    label: string;
    name: string;
}
