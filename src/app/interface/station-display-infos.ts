export interface StationDisplayInfos {
    direction: string;
    code: string;
    network: string;
    color: string;
    physical_mode: string;
}
