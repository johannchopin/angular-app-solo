import { Coord } from './coord';
import { Addresse } from './addresse';

export interface PoiType {
    id: string;
    name: string;
}


export interface Poi {
    id: string;
    poi_type: PoiType;
    name: string;
    coord: Coord;
    label: string;
    address: Addresse;
}
