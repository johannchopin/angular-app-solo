import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { CompaniesComponent } from './page/companies/companies.component';
import { ArrivalsComponent } from './page/arrivals/arrivals.component';
import { DeparturesComponent } from './page/departures/departures.component';
import { ContactsComponent } from './page/contacts/contacts.component';
import { Page404Component } from './page/page404/page404.component';


const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'company', component: CompaniesComponent },
    { path: 'contacts', component: ContactsComponent },
    { path: 'arrivals/:coords', component: ArrivalsComponent },
    { path: 'departures/:coords', component: DeparturesComponent },
    { path: '**', component: Page404Component }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
