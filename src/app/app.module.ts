import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// IMPORT LIBRARIES
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as $ from 'jquery';
import * as bootstrap from 'bootstrap';
// END IMPORT LIBRARIES

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './page/home/home.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { StationComponent } from './component/station/station.component';
import { CompanyComponent } from './component/company/company.component';
import { FooterComponent } from './component/footer/footer.component';
import { CompaniesComponent } from './page/companies/companies.component';
import { PoiComponent } from './component/poi/poi.component';
import { ArrivalsComponent } from './page/arrivals/arrivals.component';
import { StationDisplayInfosComponent } from './component/station-display-infos/station-display-infos.component';
import { DeparturesComponent } from './page/departures/departures.component';
import { ContactsComponent } from './page/contacts/contacts.component';
import { Page404Component } from './page/page404/page404.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        NavbarComponent,
        StationComponent,
        CompanyComponent,
        FooterComponent,
        CompaniesComponent,
        PoiComponent,
        ArrivalsComponent,
        StationDisplayInfosComponent,
        DeparturesComponent,
        ContactsComponent,
        Page404Component
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FontAwesomeModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
